# SiteUtils plugin voor Craft CMS 3.x

Alle basis functies voor PixelDeluxe websites.

## Base
------------------------------------------------------------------------------------------

### ArrayList
Een ArrayList object helpt je met het onderhouden van je array's.

* get(int index) : mixed => Hiermee kun je een waarde op een bepaalde index opvragen.
* add(mixed value) : mixed => Voegt de waarde toe aan de array op de volgende index.
* set(int index, mixed value) : mixed => Voegt de waarde toe op de meegegeven index.
* merge(array array) : array => Voeg de array samen met de lijst.
* remove(mixed value, bool strict = true) : mixed => Verwijdert de waarde.
* removeIndex(int index) : mixed => Verwijdert de waarde op de meegegeven index.
* contains(mixed value) : bool => Hiermee kun je controleren of de waarde bestaat.
* containsIndex(int index) : bool => Hiermee kun je controleren of de index bestaat.
* getValues() : array => Hiermee kunt je alle waardes opvragen.
* isEmpty() : bool => Hiermee kun je controleren of de lijst leeg is.

### Counter
Een counter object helpt je met het bijhouden van getallen.

* get() : number => Hiermee kun je de huidige waarde opgeven.
* increment() : Counter => Verhoogt te waarde met 1.
* decrement() : Counter => Verlaagt te waarde met 1.
* add(number value) : Counter => Verhoogt te waarde met value.
* subtract(number value) : Counter => Verlaagt te waarde met value.
* set(number value) : Counter => Stelt de waarde in op value.
* reset() : Counter => Stelt de waarde in op de standaard waarde.

### HashMap
Een HashMap object helpt je met het onderhouden van je key/value arrays.

* get(mixed key) : mixed => Hiermee kun je de waarde op van een key opvragen.
* set(mixed key, mixed value) : mixed => Stelt de waarde in op de meegegeven key.
* getOrSet(mixed key, mixed default) : mixed => Geeft de waarde van een key terug of stelt de default in.
* getOrElse(mixed key, mixed default) : mixed => Geeft de waarde van de key terug of de default.
* merge(array array) : array => Voeg de array samen met de map.
* remove(mixed value, bool strict = true) : mixed => Verwijdert de waarde.
* removeKey(mixed key) : mixed => Verwijdert de waarde op de meegegeven key.
* containsKey(mixed key) : bool => Hiermee kun je controleren of de key bestaat.
* getMap() : array => Geeft de map array terug.
* getKeys() : array => Geeft een array van keys terug.
* getValues() array => Geef een array van waardes terug.
* isEmpty() : bool => Hiermee kun je controleren of de map leeg is.

### PdfBuilder
Een PdfBuilder object helpt je met het bouwen van een pdf, zie FPDF documentatie.

### Timer
Een Timer object helpt je memt het bijhouden van tijd.

* start() : void => Start de timer.
* stop() : void => Stopt de timer.
* getTime() : float => De tijd tussen het starten en stoppen.

## Behaviors
------------------------------------------------------------------------------------------

### LinkBehavior
Voegt isForm() en getForm() toe aan linktypefield's. Zo kun je makkelijk controleren of
een link een FreeForm formulier is en het formulier object verkrijgen.

### WebUserBehavior
Voegt getIsImpersonation() en getImpersonator() toe aan gebruikers. Zo kun je makkelijk
controleren of een gebruiker een impersonator is en wie de echte gebruiker is.

## Events
------------------------------------------------------------------------------------------

### EventListener
Om event listeners overzichtelijker te maken kun je de EventListener API gebruiken. Je maakt
een class aan, laat deze EventListener extenden en implementeert de vereiste methodes. Je registreert
de listener door in de plugin init functie `EventListenerClass::init()` aan te roepen.

## Helpers
------------------------------------------------------------------------------------------

###  ArrayHelper
Dit is een uitbreiding op de Craft ArrayHelper. Extra methodes: filterObject(), findObject()

```php
$entry = ArrayHelper::findObject($entries, 'id', 5);

if($entry !== null) {
	echo "Entry title is: $entry->title";
}
```

### ClassHelper
Hiermee kun je makkelijker met classes en objecten werken.

### FieldHelper
Hiermee kun je makkelijk de waardes van single/multi option velden, zoals checkboxes en radio buttons, opvragen.

###  JsonHelper
Hier zitten tools voor json in. Je kunt gebruik maken van de volgende (static) methodes: decode(), isValid(), getProperty().

```php
$str = 'dit is geen json';

if(!JsonHelper::isValid($str)) {
	echo 'ongeldige json!';
}

$json = JsonHelper::decode('{ "mijnKey": "hallo" }');

if(!$json) {
	echo 'ongeldige json!';
	return;
}

echo 'Mijn value: ' JsonHelper::getProperty($json, 'mijnKey');
```

###  MailHelper
Gebruik deze om makkelijk simpele tekst of custom template emails te versturen. Je kunt
gebruik maken van de volgende (static) methodes; sendTextMail() en sendTemplateMail.

```php
$mailSent = MailHelper::sendTemplateMail(
    'info@pixeldeluxe.nl',
    "Hallo!", 
    "my/template", 
    ['variable' => 'for template']
);

if($mailSent) {
	echo 'Yay!';
} else {
	echo ':(';
}
```

###  MapHelper
Hier zitten tools voor HashMap in. Je kunt gebruik maken van de volgende (static) methodes; createFromArray().

```php
$entries = Entry::find()->all();
$map = MapHelper::createFromArray($entries);

if($map->get(1)) {
	echo 'Entry met ID 1 bestaat!';
}
```

###  NumberHelper
Hier zitten tools voor nummers in. Je kunt gebruik maken van de volgende (static) methodes; isInt().

```php
$valid = NumberHelper::isInt('2');

if($valid) {
	echo 'geldig nummer!';
} else {
	echo 'ongeldig nummer :(';
}
```

###  TaskHelper
Gebruik deze om taak/callable gerelateerde zaken. Zoals het veilig uitvoeren van een callable.

```php
TaskHelper::executeSafe(fn() => unsafe_function(), fn($e) => echo $e->getMessage());
```

###  ZipHelper
Hiermee kun je een bepaald bestand/een bepaalde map zippen naar een bepaalde locatie. Je kunt gebruik 
maken van de volgende (static) methodes; zip().

```php
$ignored = ['ignore', 'these', 'files', 'or', 'folders'];

// Create zip file at location
try {
    ZipHelper::zip('my/folder', 'zip/here/please.zip', true, $ignored);
} catch(Exception $e) {
    echo 'error: ' . $e->getMessage();
}

echo 'It worked :D';
```

## Utilities
------------------------------------------------------------------------------------------

###  Clear cache - public resources
Om de snelheid van de website te vergroten worden zoveel mogelijk bestanden gecached. Deze cache kan gecleared worden d.m.v. een optie in de clear cache utility. Om het clearen van de cache voor een bepaald bestand mogelijk te maken moet er een
query string achter de src geplaatst worden.

```html
<link rel="stylesheet" type="text/css" href="/public/css/main.min.css{{ cacheSuffix() }}">
```

### Logger
De logger is een centrale plek waar custom plugins logs kunnen opslaan. Daarnaast worden alle opgevangen exceptions hier gelogged.

```php
class RegisterLogTypeListener extends EventListener {
    
    public function getEventClass() {
        return Logger::class;
    }
    
    public function getEventName() {
        return Logger::EVENT_REGISTER_LOG_TYPES;
    }
    
	public function onEvent(Event $event) {
        $event->logTypes[] = new LogTypeModel(['handle' => LogType::CUSTOM, 'label' => 'Custom', 'color' => '#8BAF40']);
	}

}

SiteUtils::getInstance()->logger->log(LogType::CUSTOM, "Test log", "Test description", ["any" => "Data value"]);
```

## Validators
------------------------------------------------------------------------------------------

### DateTimeValidator
De datetime validator gebruik de Craft validator als basis maar voegt een minimale en maximale waarde optie toe.

### EmailAliasValidator
De email validator gebruik de Craft validator als basis maar voegt alias support toe.

### IntegerAliasValidator
De integer validator gebruik de Craft validator als basis maar voegt alias support toe.

### UrlAliasValidator
De url validator gebruik de Craft validator als basis maar voegt alias support toe.

##  ApiRequest & ApiResponse
Hiermee kun je op een makkelijke object georienteerde manier met API's communiceren.

```php
$apiRequest = new ApiRequest('https://my.api/doSomething');

$apiRequest->setMethod('POST');

$apiRequest->addData([
    "test1" => 'Hi',
    "test2" => 'There'
]);

// Send response
$response = $apiRequest->send();

if($response->getStatus() == 200) {
	$json = $response->getJson();
	echo $json->greeting;
} else {
	echo 'error';
}
```

## Twig
------------------------------------------------------------------------------------------

### Filters

#### base64_encode en base64_decode
Met deze filters kun je een string base64 encoden en decoden.

```twig
{% set encoded = "test"|base64_encode %}
{% set decoded = encoded|base64_decode %}
```

#### hash
Met deze filter kun je een string hashen. Deze filter maakt gebruik van de PHP hash functie.

```twig
{% set hashed = "secure"|hash("md5") %}
```

#### boolean
Met deze filter kun je een waarde omzetten in een boolean. Deze filter maakt gebruik van de PHP boolval functie.

```twig
{% set boolValue = "true"|boolean %}
```

#### parseEnv
Met deze filter kun je env variables parsen. Deze filter maakt gebruik van de Craft parseEnv functie.

```twig
{% set baseUrl = "$BASE_URL"|parseEnv %}
```

#### float
Met deze filter kun je een waarde omzetten in een float. Deze filter maakt gebruik van de PHP floatval functie.

```twig
{% set floatValue = "5.5"|float %}
```

#### integer
Met deze filter kun je een waarde omzetten in een integer. Deze filter maakt gebruik van de PHP intval functie.

```twig
{% set intValue = "15"|integer %}
```

####  reindex
Gebruik deze filter om de keys van een object of array te hernoemen. Je kunt ook een array
met objecten en/of arrays hernoemen.

```twig
{% set array1 = [
	{ field1: 'test1', field2: 'test2' },
	{ field1: 'test3', field2: 'test4' } 
] %}

{% set array2 = array1|reindex({
	field1: 'title',
	field2: 'subTitle'
}) %}

{% for item in array2 %}
	<h5>{{ item.title }}</h5>
	<p>{{ item.subTitle }}</p>
{% endfor %}
```

### Functions

#### arrayList()
This function creates an ArrayList object and returns it.

#### counter(number = 0)
This function creates an Counter object and returns it.

#### createPdf()
This function creates an PdfBuilder object and returns it.

#### hashMap()
This function creates an HashMap object and returns it.

#### hashSet()
This function creates an HashSet object and returns it.

#### hasPermission(permission)
Allows you too check whether an user has the specified permission.

#### setProperty(object, property, value)
Sets the properties value on the specified object.

### Token parsers

####  requireUserGroup
Gebruik deze functie om toegang te beperken tot specifieke gebruikersgroepen. Gebruikers die in geen
van de usergroups zitten krijgen een 403 te zien.

```twig
{% requireUserGroup('klant', 'redactie') %}
```