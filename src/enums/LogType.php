<?php
namespace pixeldeluxe\siteutils\enums;

class LogType {

    const INFO = "info";
    const ERROR = "error";
    const WARNING = "warning";
    const EXCEPTION = "exception";
    const DEBUG = "debug";

}
