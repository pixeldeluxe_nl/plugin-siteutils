<?php
namespace pixeldeluxe\siteutils\migrations;

use Craft;
use craft\db\Query;
use craft\db\Migration;
use pixeldeluxe\siteutils\db\Table;

class m200302_150632_logger_extra_data_conversion extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $logs = (new Query())
            ->select(['id', 'extra', 'dateCreated'])
            ->from([Table::LOGS])
            ->where(['<','dateCreated', '2020-01-17'])
            ->all();

        foreach ($logs as $log) {
            $decoded = base64_decode($log['extra']);

            if($decoded === false) {
                continue;
            }

            $uncompressed = @gzuncompress($decoded);

            if($uncompressed === false) {
                $this->_updateLog($log['id'], $decoded);
                continue;
            }

            $this->_updateLog($log['id'], $uncompressed);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m200302_150632_logger_extra_data_conversion cannot be reverted.\n";
        return false;
    }

    /*
     * Updates the specified log.
     *
     * @param $id
     * @param $extra
     */
    private function _updateLog($id, $extra) {
        $this->update(Table::LOGS, [ 'extra' => $extra ], [ 'id' => $id ]);
    }

}
