<?php
namespace pixeldeluxe\siteutils\migrations;

use Craft;
use craft\db\Migration;
use pixeldeluxe\siteutils\db\Table;

class Install extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTables();
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->removeTables();
        return true;
    }

    /**
     * Create the SiteUtils tables.
     */
    protected function createTables() {
        $this->createTable(
            Table::CACHE_TOKEN, [
                'id' => $this->primaryKey(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
                
                'token' => $this->char(10)->notNull()
            ]
        );

        $this->createTable(
            Table::LOGS, [
                'id' => $this->primaryKey(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
                
                'type' => $this->string(255)->notNull(),
                'title' => $this->string(255)->notNull(),
                'description' => $this->text(),
                'extra' => $this->text(),
                'archived' => $this->boolean()->defaultValue(false)->notNull()
            ]
        );
    }

    /**
     * Remove the SiteUtils tables.
     */
    protected function removeTables() {
        $this->dropTableIfExists(Table::CACHE_TOKEN);
    }

}
