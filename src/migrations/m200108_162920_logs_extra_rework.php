<?php

namespace pixeldeluxe\siteutils\migrations;

use craft\db\Migration;
use pixeldeluxe\siteutils\db\Table;

/**
 * m200108_162920_logs_extra_rework migration.
 */
class m200108_162920_logs_extra_rework extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->alterColumn(Table::LOGS, 'extra', $this->longText());
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m200108_162920_logs_extra_rework cannot be reverted.\n";
        return false;
    }

}
