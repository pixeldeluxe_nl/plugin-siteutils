<?php
namespace pixeldeluxe\siteutils\migrations;

use craft\db\Migration;
use pixeldeluxe\siteutils\db\Table;

class m190819_085432_client_cache_rework extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->dropTableIfExists(Table::CACHE_TOKEN);

        $this->createTable(
            Table::CACHE_TOKEN, [
                'id' => $this->primaryKey(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
                
                'token' => $this->char(10)->notNull()
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m190819_085432_client_cache_rework cannot be reverted.\n";
        return false;
    }

}

