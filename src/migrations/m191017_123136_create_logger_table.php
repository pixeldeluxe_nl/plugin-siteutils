<?php
namespace pixeldeluxe\siteutils\migrations;

use Craft;
use craft\db\Migration;
use pixeldeluxe\siteutils\db\Table;

class m191017_123136_create_logger_table extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->dropTableIfExists(Table::LOGS);

        $this->createTable(
            Table::LOGS, [
                'id' => $this->primaryKey(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
                
                'type' => $this->string(255)->notNull(),
                'title' => $this->string(255)->notNull(),
                'description' => $this->text(),
                'extra' => $this->text()
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m191017_123136_create_logger_table cannot be reverted.\n";
        return false;
    }

}
