<?php
namespace pixeldeluxe\siteutils\migrations;

use craft\db\Migration;
use pixeldeluxe\siteutils\db\Table;

class m200221_152637_logger_archive_option extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addColumn(Table::LOGS, 'archived', $this->boolean()->defaultValue(false)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        echo "m200221_152637_logger_archive_option cannot be reverted.\n";
        return false;
    }

}
