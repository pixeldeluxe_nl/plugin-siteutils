<?php
namespace pixeldeluxe\siteutils\validators;

use yii\validators\EmailValidator;
use Craft;

class EmailAliasValidator extends EmailValidator {

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute) {
        $value = Craft::parseEnv($model->$attribute);
        $errors = $this->validateValue($value);
        $error = $errors ? reset($errors) : null;

        // Ignore if skipOnEmpty and empty
        if($this->skipOnEmpty && $this->isEmpty($value)) {
            return;
        }

        // Add the error
        if($error !== null) {
        	$this->addError($model, $attribute, $error);
        }
    }

}