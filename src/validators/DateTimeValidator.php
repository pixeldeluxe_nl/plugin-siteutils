<?php
namespace pixeldeluxe\siteutils\validators;

use Craft;
use craft\helpers\DateTimeHelper;

class DateTimeValidator extends \craft\validators\DateTimeValidator {

    public $min;
    public $tooSmall;

    public $max;
    public $tooBig;

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute) {
        parent::validateAttribute($model, $attribute);

        // Get value
        $value = $model->$attribute;

        // Make sure its not too small
        if($this->min !== null) {
            $min = DateTimeHelper::toDateTime($this->min);

            if($value < $min) {
                $this->addError($model, $attribute, $this->tooSmall);
            }
        }

        // Make sure its not too big
        if($this->max !== null) {
            $max = DateTimeHelper::toDateTime($this->max);

            if($value > $max) {
                $this->addError($model, $attribute, $this->tooBig);
            }
        }
    }

}
