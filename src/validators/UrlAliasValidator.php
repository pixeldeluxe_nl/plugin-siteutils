<?php
namespace pixeldeluxe\siteutils\validators;

use Craft;
use craft\validators\UrlValidator;

class UrlAliasValidator extends UrlValidator {

    /**
     * @inheritdoc
     */
    public function validateValue($value) {
    	return parent::validateValue(Craft::parseEnv($value));
    }

}