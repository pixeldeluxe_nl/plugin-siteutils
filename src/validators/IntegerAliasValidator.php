<?php
namespace pixeldeluxe\siteutils\validators;

use yii\validators\Validator;
use Craft;

class IntegerAliasValidator extends Validator {

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute) {
        $value = Craft::parseEnv($model->$attribute);

        if(!is_numeric($value)) {
        	$this->addError($model, $attribute, "{attribute} moet een nummer zijn.");
        }
    }

}