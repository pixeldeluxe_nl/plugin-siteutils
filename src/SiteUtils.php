<?php
namespace pixeldeluxe\siteutils;

use Craft;
use craft\base\Plugin;
use http\Client;
use pixeldeluxe\siteutils\behaviors\WebUserBehavior;
use pixeldeluxe\siteutils\services\ClientCache;
use pixeldeluxe\siteutils\services\ContextStorage;
use pixeldeluxe\siteutils\services\Logger;
use pixeldeluxe\siteutils\events\listeners\BeforeHandleExceptionListener;
use pixeldeluxe\siteutils\events\listeners\RegisterCacheOptionsListener;
use pixeldeluxe\siteutils\events\listeners\RegisterCpAlertsListener;
use pixeldeluxe\siteutils\events\listeners\RegisterUtilitiesListener;
use pixeldeluxe\siteutils\events\listeners\InitVariablesListener;
use pixeldeluxe\siteutils\events\listeners\RegisterLinkBehaviorListener;
use pixeldeluxe\siteutils\events\listeners\RegisterLinkTypeListener;
use pixeldeluxe\siteutils\web\twig\Extension;
use typedlinkfield\Plugin as LinkTypeField;
use Solspace\Freeform\Freeform;

/**
 * @property Logger $logger
 * @property ContextStorage $contextStorage
 * @property ClientCache $clientCache
*/
class SiteUtils extends Plugin {

    /**
     * @inheritdoc
     */
	public function init() {
		parent::init();

        // Attach WebUserBehavior to web user 
        $webUser = Craft::$app->getUser();

        if(!$webUser->getBehavior('siteUtilsBehavior')) {
            $webUser->attachBehavior('siteUtilsBehavior', WebUserBehavior::class);
        }

		// Register twig extentions
		$extension = new Extension($this);
		Craft::$app->view->registerTwigExtension($extension);

        // Register components and plugins
		$this->_registerComponents();
		$this->_registerEvents();
	}

    /**
     * Register all plugin components.
     */
    private function _registerComponents() {
        $this->setComponents([
            'clientCache' => ClientCache::class,
            'contextStorage'   => ContextStorage::class,
            'logger' => Logger::class
        ]);
    }

    /**
     * Register all plugin events.
     */
    private function _registerEvents() {
        BeforeHandleExceptionListener::init();
        RegisterCacheOptionsListener::init();
        RegisterCpAlertsListener::init();
        RegisterUtilitiesListener::init();
        InitVariablesListener::init();

        // Add FreeForm link type
        if(class_exists(Freeform::class) && class_exists(LinkTypeField::class)) {
            RegisterLinkBehaviorListener::init();
            RegisterLinkTypeListener::init();
        }
    }

}