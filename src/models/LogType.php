<?php
namespace pixeldeluxe\siteutils\models;

use craft\base\Model;

class LogType extends Model {
	
	public $handle;
	public $label;
	public $color;

}