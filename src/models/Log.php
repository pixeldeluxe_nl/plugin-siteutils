<?php
namespace pixeldeluxe\siteutils\models;

use JsonSerializable;
use craft\base\Model;

class Log extends Model implements JsonSerializable {
	
	public $id;
	public $type;
	public $title;
	public $description;
	public $extra;
    public $dateCreated;
    public $archived;

    /**
     * @inheritdoc
     * Override the way this model is converted to json.
     */
    public function jsonSerialize() {
        return [
        	'id' => $this->id,
        	'type' => $this->type,
        	'title' => $this->title,
        	'description' => $this->description,
        	'extra' => $this->extra,
        	'dateCreated' => $this->dateCreated->format('d-m-Y H:i:s'),
            'archived' => $this->archived
        ];
    }

}