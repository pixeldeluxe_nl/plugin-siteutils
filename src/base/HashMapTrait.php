<?php
namespace pixeldeluxe\siteutils\base;

trait HashMapTrait {

    /**
     * Returns the item by key. Returns null 
     * if it does not exist.
     *
     * @param mixed $key
     * @return mixed|null
     */
	public function get($key) {
		if(!$this->containsKey($key)) return null;
		return $this->map[$this->_safeKey($key)];
	}

    /**
     * Sets the value by key and returns
     * the value.
     *
     * @param mixed $key
     * @param mixed $value
     * @return mixed
     */
	public function set($key, $value) {
		$this->map[$this->_safeKey($key)] = $value;
		return $value;
	}

    /**
     * Returns the item by key. Sets and 
     * returns the default if it does not exist.
     *
     * @param mixed $key
     * @param mixed $default
     * @return mixed
     */
	public function getOrSet($key, $default) {
		return $this->get($key) ?? $this->set($key, $default);
	}

    /**
     * Returns the item by key. Returns 
     * the default if it does not exist.
     *
     * @param mixed $key
     * @param mixed $default
     * @return mixed
     */
	public function getOrElse($key, $default) {
		return $this->get($key) ?? $default;
	}

    /**
     * Merges another array into this map and
     * then returns the merged list.
     *
     * @param array $array
     * @return array
     */
	public function merge(array $array) : array {
		$this->map = array_merge($this->map, $array);
		return $array;
	}

    /**
     * Removed the specified value from the map
     * and then returns the removed value.
     *
     * @param mixed $value
     * @param bool $strict
     * @return mixed
     */
	public function remove($value, bool $strict = true) {
		$this->map = array_filter($this->map,
		    function($i) use ($value, $strict) {
		        return $strict ? ($i !== $value) : ($i != $value);
		    }
		);
		return $value;
	}

    /**
     * Removes the value by key and then 
     * returns the removed value.
     *
     * @param mixed $key
     * @return mixed
     */
	public function removeKey($key) {
        if(!$this->containsKey($key)) {
            return null;
        }

        // Remove value
		$value = $this->map[$this->_safeKey($key)];
		unset($this->map[$this->_safeKey($key)]);
		return $value;
	}

    /**
     * Returns if the specified key exists within the map.
     *
     * @param mixed $key
     * @return bool
     */
	public function containsKey($key) : bool {
		return array_key_exists($this->_safeKey($key), $this->map);
	}

    /**
     * Returns the raw map.
     *
     * @return array
     */
	public function getMap() : array {
		return $this->map;
	}

    /**
     * Returns the keys.
     *
     * @return array
     */
	public function getKeys() : array {
		return array_keys($this->map);
	}

    /**
     * Returns the values.
     *
     * @return array
     */
	public function getValues() : array {
		return array_values($this->map);
	}

    /**
     * Returns whether the map is empty.
     *
     * @return bool
     */
	public function isEmpty() : bool {
		return count($this->map) <= 0;
	}

    /**
     * @inheritdoc
     */
    public function jsonSerialize() {
        return $this->map;
    }

    private function _safeKey($key) : string {
        return (string) $key;
    }

}