<?php
namespace pixeldeluxe\siteutils\base;

class ArrayList implements \JsonSerializable {
	
	protected $list;

    /**
     * Constructor
     *
     * @param array $list
     */
	public function __construct($list = []) {
		$this->list = $list;
	}

    /**
     * Returns the item on the specified index.
     * Returns null if it does not exist.
     *
     * @param int $index
     * @return mixed|null
     */
	public function get(int $index) {
		if(!$this->containsIndex($index)) return null;
		return $this->list[$index];
	}

    /**
     * Adds the specified value to the list and
     * then returns the added value.
     *
     * @param mixed $value
     * @return mixed
     */
	public function add($value) {
		array_push($this->list, $value);
		return $value;
	}

    /**
     * Sets the value of the specified index.
     *
     * @param int $index
     * @param mixed $value
     * @return mixed
     */
	public function set(int $index, $value) {
		$this->list[$index] = $value;
		return $value;
	}

    /**
     * Merges another array into this list and
     * then returns the merged list.
     *
     * @param array $array
     * @return array
     */
	public function merge(array $array) {
		$this->list = array_merge($this->list, $array);
		return $array;
	}

    /**
     * Removed the specified value from the list
     * and then returns the removed value.
     *
     * @param mixed $value
     * @param bool $strict
     * @return mixed
     */
	public function remove($value, bool $strict = true) {
		$this->list = array_filter($this->list,
		    function($i) use ($value, $strict) {
		        return $strict ? ($i !== $value) : ($i != $value);
		    }
		);
		return $value;
	}

    /**
     * Removes the value on the specified index
     * and then returns the removed value.
     *
     * @param int $index
     * @return mixed
     */
	public function removeIndex(int $index) {
        if(!$this->containsIndex($index)) {
            return null;
        }

        // Remove value
		$value = $this->list[$index];
		unset($this->list[$index]);
		return $value;
	}

    /**
     * Returns whether this list contains the specified value.
     *
     * @param mixed $value
     * @return bool
     */
    public function contains($value) : bool {
        return in_array($value, $this->list);
    }

    /**
     * Returns whether the specified index exists within the list.
     *
     * @param int $index
     * @return bool
     */
	public function containsIndex(int $index) : bool {
		return array_key_exists($index, $this->list);
	}

    /**
     * Returns the values of the list.
     *
     * @return array
     */
	public function getValues() : array {
		return array_values($this->list);
	}

    /**
     * Returns whether the list is empty.
     *
     * @return bool
     */
	public function isEmpty() : bool {
		return count($this->list) <= 0;
	}

    /**
     * @inheritdoc
     */
    public function jsonSerialize() {
        return $this->list;
    }

}