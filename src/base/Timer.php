<?php
namespace pixeldeluxe\siteutils\base;

class Timer {

	private $startTime;
	private $endTime;

    /**
     * Starts the timer.
     */
	public function start() {
		$this->startTime = round(microtime(true) * 1000);
	}

    /**
     * Stops the timer and returns the time measured.
     * 
     * @return float
     */
	public function stop() {
		$this->endTime = round(microtime(true) * 1000);
        return $this->getTime();
	}

    /**
     * Returns the time measured.
     *
     * @return float
     */
	public function getTime() : float {
		return ($this->endTime - $this->startTime);
	}

}