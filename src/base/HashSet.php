<?php
namespace pixeldeluxe\siteutils\base;

class HashSet extends ArrayList {
	
	/**
	 * @inheritdoc
	 */
	public function add($value) {
		if(in_array($value, $this->list)) {
			return $value;
		}

		return parent::add($value);
	}

	/**
	 * @inheritdoc
	 */
	public function set(int $index, $value) {
		if(in_array($value, $this->list)) {
			return $value;
		}

		return parent::set($index, $value);
	}

	/**
	 * @inheritdoc
	 */
	public function merge(array $array) {
		$this->list = array_unique(array_merge($this->list, $array), SORT_REGULAR);
		return $array;
	}
	
}