<?php
namespace pixeldeluxe\siteutils\base;

class Counter {
	
	private $initialValue;
	private $number;

    /**
     * Constructor
     *
     * @param number $number
     */
	public function __construct($number = 0) {
		$this->number = $number;
		$this->initialValue = $number;
	}

    /**
     * Returns the current number.
     *
     * @return number
     */
	public function get() {
		return $this->number;
	}

    /**
     * Increases the current number with one.
     *
     * @return Counter
     */
	public function increment() : Counter {
		$this->number++;
		return $this;
	}

    /**
     * Decreases the current number with one.
     *
     * @return Counter
     */
	public function decrement() : Counter {
		$this->number--;
		return $this;
	}

    /**
     * Adds the specified number to the current number.
     *
     * @return Counter
     */
	public function add($amount) : Counter {
		$this->number += $amount;
		return $this;
	}

    /**
     * Subtracts the specified number to the current number.
     *
     * @return Counter
     */
	public function subtract($amount) : Counter {
		$this->number -= $amount;
		return $this;
	}

    /**
     * Sets the current number.
     *
     * @return Counter
     */
	public function set($amount) : Counter {
		$this->number = $amount;
		return $this;
	}

    /**
     * Resets the current number to the initial number.
     *
     * @return Counter
     */
	public function reset() : Counter {
		$this->number = $this->initialValue;
		return $this;
	}

}