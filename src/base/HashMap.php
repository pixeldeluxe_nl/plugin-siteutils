<?php
namespace pixeldeluxe\siteutils\base;

class HashMap implements \JsonSerializable {
	
	use HashMapTrait;

	protected $map;

    /**
     * Constructor
     *
     * @param array $map
     */
	public function __construct(array $map = []) {
		$this->map = $map;
	}

}