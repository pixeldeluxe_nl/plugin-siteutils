<?php
namespace pixeldeluxe\siteutils\controllers;

use yii\web\Response as YiiResponse;
use yii\web\BadRequestHttpException;
use Craft;
use craft\web\Controller;
use pixeldeluxe\siteutils\SiteUtils;

class LoggerController extends Controller {

    /**
     * Returns all logs as json.
     *
     * @return YiiResponse
     */
    public function actionGetLogs() : YiiResponse {
        $request = Craft::$app->getRequest();

        // Validate request
        $this->requirePermission('utility:logger');
        $this->requireAcceptsJson();

        // Get input
        $limit = $request->getRequiredQueryParam('limit');
        $offset = $request->getRequiredQueryParam('offset');
        $criteria = $request->getParam('criteria', []);

        if(!is_array($criteria)) {
            throw new BadRequestHttpException('Criteria must be an array');
        }

        // Get logs
        $logs = SiteUtils::getInstance()->logger->getLogs($limit, $offset, $criteria);

        return $this->asJson([
            'data' => $logs,
            'total' => SiteUtils::getInstance()->logger->getTotal($criteria)
        ]);
    }

    /**
     * Archive all logs or logs from a specific type.
     *
     * @return YiiResponse
     */
    public function actionArchiveLogs() : YiiResponse {
        $request = Craft::$app->getRequest();

        // Validate request
        $this->requirePermission('utility:logger');
        $this->requirePostRequest();
        $this->requireAcceptsJson();

        // Get input
        $criteria = $request->getParam('criteria', []);

        if(!is_array($criteria)) {
            throw new BadRequestHttpException('Criteria must be an array');
        }

        // Perform action
        $affectedRows = SiteUtils::getInstance()->logger->archiveLogs($criteria);

        // Respond
        return $this->asJson([
            'rows' => $affectedRows
        ]);
    }

    /**
     * Delete all logs or logs from a specific type.
     *
     * @return YiiResponse
     */
    public function actionDeleteLogs() : YiiResponse {
        $request = Craft::$app->getRequest();

        // Validate request
        $this->requirePermission('utility:logger');
        $this->requirePostRequest();
        $this->requireAcceptsJson();

        // Get input
        $criteria = $request->getParam('criteria', []);

        if(!is_array($criteria)) {
            throw new BadRequestHttpException('Criteria must be an array');
        }

        // Perform action
        $affectedRows = SiteUtils::getInstance()->logger->deleteLogs($criteria);

        // Respond
        return $this->asJson([
            'rows' => $affectedRows
        ]);
    }

}