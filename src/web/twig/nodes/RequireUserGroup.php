<?php
namespace pixeldeluxe\siteutils\web\twig\nodes;

use yii\web\ForbiddenHttpException;
use Twig\Compiler;
use Twig\Node\Node;
use Craft;

class RequireUserGroup extends Node {

    /**
     * @inheritdoc
     */
    public function compile(Compiler $compiler) {
        $compiler
        	->write(RequireUserGroup::class . "::requireUserGroup(")
            ->subcompile($this->getNode('groups'))
            ->raw(");\n");
    }

    public static function requireUserGroup($groups) {
		$userSession = Craft::$app->getUser();
		$user = $userSession->getIdentity();

		// Require login
        if ($userSession->getIsGuest()) {
            $userSession->loginRequired();
            Craft::$app->end();
        }

        // Check user groups
        if(!$user->admin) {
            $groups = is_array($groups) ? $groups : [$groups];

            foreach ($groups as $group) {
                if($user->isInGroup($group)) {
                    return;
                }
            }

            throw new ForbiddenHttpException();
        }
    }

}