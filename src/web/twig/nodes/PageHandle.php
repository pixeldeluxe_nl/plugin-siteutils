<?php
namespace pixeldeluxe\siteutils\web\twig\nodes;

class PageHandle extends \Twig_Node {

    /**
     * @inheritdoc
     */
    public function compile(\Twig_Compiler $compiler) {
        $compiler
            ->write('$context[\''. "page_handle" .'\'] = ')
            ->subcompile($this->getNode('value'))
            ->raw(";\n")
        ;
    }
    
}