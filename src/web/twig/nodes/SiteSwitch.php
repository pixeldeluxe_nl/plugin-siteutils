<?php
namespace pixeldeluxe\siteutils\web\twig\nodes;

class SiteSwitch extends \Twig_Node {

    /**
     * @inheritdoc
     */
    public function compile(\Twig_Compiler $compiler) {
        $compiler
            ->write('$context[\''. "lang_switch_params" .'\'] = ')
            ->subcompile($this->getNode('value'))
            ->raw(";\n")
        ;
    }
    
}