<?php
namespace pixeldeluxe\siteutils\web\twig\tokenparsers;

use pixeldeluxe\siteutils\web\twig\nodes\SiteSwitch as SiteSwitchNode;

class SiteSwitch extends \Twig_TokenParser {

    /**
     * @inheritdoc
     */
	public function parse(\Twig_Token $token) {
        $parser = $this->parser;
        $stream = $parser->getStream();

        $value = $parser->getExpressionParser()->parseExpression();
        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return new SiteSwitchNode(array('value' => $value), array(), $token->getLine(), $this->getTag());
	}

    /**
     * @inheritdoc
     */
	public function getTag() {
		return 'setSwitchParams';
	}

}