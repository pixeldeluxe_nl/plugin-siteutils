<?php
namespace pixeldeluxe\siteutils\web\twig\tokenparsers;

use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;
use pixeldeluxe\siteutils\web\twig\nodes\RequireUserGroup as RequireUserGroupNode;

class RequireUserGroup extends AbstractTokenParser {

    /**
     * @inheritdoc
     */
    public function parse(Token $token) {
        $lineno = $token->getLine();
        $parser = $this->parser;
        $stream = $parser->getStream();

        $nodes = [
            'groups' => $parser->getExpressionParser()->parseExpression(),
        ];
        $stream->expect(Token::BLOCK_END_TYPE);
        return new RequireUserGroupNode($nodes, [], $lineno, $this->getTag());
    }

    /**
     * @inheritdoc
     */
    public function getTag() {
        return 'requireUserGroup';
    }

}
