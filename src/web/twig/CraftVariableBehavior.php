<?php
namespace pixeldeluxe\siteutils\web\twig;

use yii\base\Behavior;
use Craft;
use pixeldeluxe\siteutils\SiteUtils;

class CraftVariableBehavior extends Behavior {

    public $siteutils;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->siteutils = SiteUtils::getInstance();
    }

}