<?php
namespace pixeldeluxe\siteutils\web\twig;

use Twig\TwigTest;

use pixeldeluxe\siteutils\web\twig\filters\ParseEnv;
use pixeldeluxe\siteutils\web\twig\filters\Reindex;
use pixeldeluxe\siteutils\web\twig\filters\CustomHash;
use pixeldeluxe\siteutils\web\twig\filters\ParseInteger;
use pixeldeluxe\siteutils\web\twig\filters\ParseFloat;
use pixeldeluxe\siteutils\web\twig\filters\ParseBoolean;
use pixeldeluxe\siteutils\web\twig\filters\Base64Encode;
use pixeldeluxe\siteutils\web\twig\filters\Base64Decode;
use pixeldeluxe\siteutils\web\twig\tokenparsers\SiteSwitch;
use pixeldeluxe\siteutils\web\twig\tokenparsers\PageHandle;
use pixeldeluxe\siteutils\web\twig\tokenparsers\RequireUserGroup;
use pixeldeluxe\siteutils\web\twig\functions\ClientCacheSuffix;
use pixeldeluxe\siteutils\web\twig\functions\HasPermission;
use pixeldeluxe\siteutils\web\twig\functions\Counter;
use pixeldeluxe\siteutils\web\twig\functions\HashMap;
use pixeldeluxe\siteutils\web\twig\functions\HashSet;
use pixeldeluxe\siteutils\web\twig\functions\ArrayList;
use pixeldeluxe\siteutils\web\twig\functions\SetProperty;
use pixeldeluxe\siteutils\web\twig\functions\CreatePdf;

class Extension extends \Twig_Extension {
    
    public function getTokenParsers() {
        return [
            new SiteSwitch(),
            new PageHandle(),
            new RequireUserGroup()
        ];
    }

    public function getFilters() {
        return [
            new ParseEnv(),
            new Reindex(),
            new CustomHash(),
            new ParseInteger(),
            new ParseFloat(),
            new ParseBoolean(),
            new Base64Encode(),
            new Base64Decode()
        ];
    }

    public function getFunctions() {
    	return array(
    		new ClientCacheSuffix(),
            new HasPermission(),
            new Counter(),
            new HashMap(),
            new HashSet(),
            new ArrayList(),
            new SetProperty(),
    	);
    }

    public function getTests() {
        return [
            new TwigTest('numeric', function($obj) {
                return is_numeric($obj);
            })
        ];
    }

}