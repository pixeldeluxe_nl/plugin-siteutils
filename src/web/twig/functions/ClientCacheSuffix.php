<?php
namespace pixeldeluxe\siteutils\web\twig\functions;

use Craft;
use pixeldeluxe\siteutils\SiteUtils;
use pixeldeluxe\siteutils\events\listeners\RegisterCacheOptionsListener;

class ClientCacheSuffix extends \Twig_Function {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('cacheSuffix', array($this, 'cacheSuffix'));
	}

    /**
     * Returns the cache suffix.
     *
     * @param bool $valueOnly Only returns the token if true
     * @return mixed|null
     */
	public function cacheSuffix(bool $valueOnly = false) {
		$symbol = $valueOnly ? "" : "?v=";
		return $symbol . SiteUtils::getInstance()->clientCache->getToken();
	}

}