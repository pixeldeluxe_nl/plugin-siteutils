<?php
namespace pixeldeluxe\siteutils\web\twig\functions;

use Craft;
use pixeldeluxe\siteutils\base\Counter as CounterObject;

class Counter extends \Twig_Function {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('counter', array($this, 'counter'));
	}

    /**
     * Creates a new Counter object with optional initial value.
     *
     * @param mixed $number Optional initial value.
     * @return CounterObject
     */
	public function counter($number = 0) : CounterObject {
		return new CounterObject($number);
	}

}