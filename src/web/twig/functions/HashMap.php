<?php
namespace pixeldeluxe\siteutils\web\twig\functions;

use pixeldeluxe\siteutils\base\HashMap as HashMapObject;

class HashMap extends \Twig_Function {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('hashMap', array($this, 'hashMap'));
	}

    /**
     * Returns a new HashMap.
     *
     * @return HashMapObject
     */
	public function hashMap() : HashMapObject {
		return new HashMapObject();
	}

}