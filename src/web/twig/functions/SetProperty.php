<?php
namespace pixeldeluxe\siteutils\web\twig\functions;

class SetProperty extends \Twig_Function {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('setProperty', array($this, 'setProperty'));
	}

    /**
     * Sets an object property.
     *
     * @param object $object
     * @param string $property
     * @param mixed $value
     */
	public function setProperty(object $object, string $property, $value) {
		$object->$property = $value;
	}

}