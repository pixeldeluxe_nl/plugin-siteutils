<?php
namespace pixeldeluxe\siteutils\web\twig\functions;

use Craft;

class HasPermission extends \Twig_Function {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('hasPermission', array($this, 'hasPermission'));
	}

    /**
     * Returns whether the current user has
     * the specified permission.
     *
     * @param string $permission
     * @return bool
     */
	public function hasPermission(string $permission) : bool {
		return Craft::$app->user->checkPermission($permission);
	}

}