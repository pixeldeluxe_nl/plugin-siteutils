<?php
namespace pixeldeluxe\siteutils\web\twig\functions;

use pixeldeluxe\siteutils\base\ArrayList as ArrayListObject;

class ArrayList extends \Twig_Function {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('arrayList', array($this, 'arrayList'));
	}

    /**
     * Returns a new ArrayList.
     *
     * @return ArrayListObject
     */
	public function arrayList() : ArrayListObject {
		return new ArrayListObject();
	}

}