<?php
namespace pixeldeluxe\siteutils\web\twig\functions;

use pixeldeluxe\siteutils\base\HashSet as HashSetObject;

class HashSet extends \Twig_Function {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('hashSet', array($this, 'hashSet'));
	}

    /**
     * Returns a new HashSet.
     *
     * @return HashSetObject
     */
	public function hashSet() : HashSetObject {
		return new HashSetObject();
	}

}