<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

class ParseBoolean extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('boolean', array($this, 'boolean'));
	}

    /**
     * Returns the boolean value.
     *
     * @param $value
     * @return bool
     */
    public function boolean($value) {
        return boolval($value);
    }

}