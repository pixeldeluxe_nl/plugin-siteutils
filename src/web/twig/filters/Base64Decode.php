<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

class Base64Decode extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('base64_decode', array($this, 'decode'));
	}

    /**
     * Returns the string value.
     *
     * @param $value
     * @return string
     */
    public function decode($value) {
        return base64_decode($value);
    }

}