<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

class Base64Encode extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('base64_encode', array($this, 'encode'));
	}

    /**
     * Returns the base64 value.
     *
     * @param $value
     * @return string
     */
    public function encode($value) {
        return base64_encode($value);
    }

}