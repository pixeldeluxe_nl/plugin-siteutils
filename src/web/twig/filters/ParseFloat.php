<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

class ParseFloat extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('float', array($this, 'float'));
	}

    /**
     * Returns the float value.
     *
     * @param $value
     * @return float
     */
    public function float($value) {
        return floatval($value);
    }

}