<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

use Craft;

class ParseEnv extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('parseEnv', array($this, 'parseEnv'));
	}

    /**
     * Returns the parsed env value.
     *
     * @param string $key
     * @return mixed|null
     */
	public function parseEnv(string $key) {
       return Craft::parseEnv($key);
	}

}