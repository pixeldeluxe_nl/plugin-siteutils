<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

class ParseInteger extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('integer', array($this, 'integer'));
	}

    /**
     * Returns the int value.
     *
     * @param $value
     * @return int
     */
    public function integer($value) {
        return intval($value);
    }

}