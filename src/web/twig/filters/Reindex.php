<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

class Reindex extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('reindex', array($this, 'reindex'));
	}

    /**
     * Reindexes the specified objects.
     *
     * @param mixed $objects
     * @param array $indexInfo A key (old key) -> value (new key) array
     * @return mixed
     */
	public function reindex($objects, array $indexInfo) {
		// Return single object if not an array
		if(!is_array($objects)) {
			return $this->_reindexObject($objects, $indexInfo);
		}

		// Build array with new objects
		$array = [];
		foreach ($objects as $object) {
			$array[] = $this->_reindexObject($object, $indexInfo);
		}

		return $array;
	}

    /**
     * Reindexe a single object.
     *
     * @param mixed $object
     * @param array $indexInfo A key (old key) -> value (new key) array
     * @return mixed
     */
	private function _reindexObject($object, array $indexInfo) {
		$values = [];

		// Create array from object values
		foreach ($indexInfo as $old => $new) {
			$values[$new] = is_array($object) ? $object[$old] : $object->$old;
		}

		return $values;
	}

}