<?php
namespace pixeldeluxe\siteutils\web\twig\filters;

class CustomHash extends \Twig_SimpleFilter {

    /**
     * Constructor
     */
	public function __construct() {
		parent::__construct('customHash', array($this, 'customHash'));
	}

    /**
     * Returns a the hashed value.
     *
     * @param string $algorithm
     * @param string $data
     * @return string
     */
    public function customHash(string $data, string $algorithm) : string {
        return hash($algorithm, $data);
    }

}