<?php
namespace pixeldeluxe\siteutils\web;

use JsonSerializable;
use yii\base\BaseObject;

class ApiResponse extends BaseObject implements JsonSerializable {

    // Status constants
    // =========================================

    const STATUS_OK = 200;
    const STATUS_CREATED = 201;

    const STATUS_BAD_REQUEST = 400;
    const STATUS_UNAUTHORIZED = 401;
    const STATUS_FORBIDDEN = 403;
    const STATUS_NOT_FOUND = 404;

    const STATUS_SERVER_ERROR = 500;


    // Error constants
    // =========================================

    const ERROR_NO_PERMISSION = [ self::STATUS_FORBIDDEN, self::STATUS_UNAUTHORIZED ];
    const ERROR_INVALID_REQUEST = [ self::STATUS_SERVER_ERROR, self::STATUS_NOT_FOUND ];
    const ERROR_SERVER_FAULT = [ self::STATUS_SERVER_ERROR ];


    // Properties
    // =========================================

	public $status;
	public $body;

    /**
     * Constructor
     *
     * @param int $status
     * @param string $body
     */
	public function __construct(int $status, string $body) {
		$this->status = $status;
		$this->body = $body;
	}

    /**
     * Returns whether the specified array
     * contains the current status.
     *
     * @param array $type An array with statuses.
     * @return bool
     */
    public function checkStatus(array $statuses) : bool {
        return in_array($this->status, $statuses);
    }

    /**
     * Returns the status.
     *
     * @return int
     */
    public function getStatus() : int {
        return $this->status;
    }

    /**
     * Returns the body.
     *
     * @return string
     */
    public function getBody() : string {
        return $this->body;
    }

    /**
     * Returns the json decoded body.
     *
     * @return mixed
     */
	public function getJson() {
		return json_decode($this->body);
	}

    /**
     * @inheritdoc
     */
    public function jsonSerialize() {
        return get_object_vars($this);
    }

}