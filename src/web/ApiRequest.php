<?php
namespace pixeldeluxe\siteutils\web;

use craft\helpers\ArrayHelper;
use craft\helpers\StringHelper;

class ApiRequest implements \JsonSerializable {
    
    // Supported methods
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_POST_FORM = 'POST-FORM';
    const METHOD_POST_FORM_URLENCODED = "POST-FORM-URLENCODED";
    const METHOD_PUT = 'PUT';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';

    const CONTENT_TYPE_JSON = 'Content-Type: application/json';

    // Supported properties
    public $url;
    public $method = "GET";
    public $headers = [];
    public $data = [];
    public $timeout = 60;
    public $connectTimeout = 10;

    /**
     * Constructor
     *
     * @param string $url
     */
    public function __construct(string $url) {
        $this->url = $url;
    }

    /**
     * Set the method.
     *
     * @param string $method
     * @return ApiRequest
     */
    public function setMethod(string $method) : ApiRequest {
        $this->method = $method;
        return $this;
    }

    /**
     * Reset the headers to the default.
     *
     * @return ApiRequest
     */
    public function resetHeaders() : ApiRequest {
        $this->headers = [];
        return $this;
    }

    /**
     * Add the specified headers. You may pass an
     * array with string headers or an associative
     * array where the key is the header name and
     * the value is the header value.
     *
     * @param array $headers
     * @return ApiRequest
     */
    public function addHeaders(array $headers) : ApiRequest {
        $this->headers = array_merge($this->headers, $this->_parseHeaders($headers));
        return $this;
    }

    /**
     * Resets the headers array and adds the
     * new headers using addHeaders().
     *
     * @param array $headers
     * @return ApiRequest
     */
    public function setHeaders(array $headers) : ApiRequest {
        $this->headers = $headers;
        return $this;
    }

    /**
     * Adds the specified data.
     *
     * @param array $data
     * @return ApiRequest
     */
    public function addData(array $data) : ApiRequest {
        foreach($data as $key => $value) {
            $this->data[$key] = $value;
        }
        
        return $this;
    }

    /**
     * Sets the data.
     *
     * @param array $data
     * @return ApiRequest
     */
    public function setData(array $data) : ApiRequest {
        $this->data = $data;
        return $this;
    }

    /**
     * Set the timeout.
     *
     * @param int $timeout
     * @return ApiRequest
     */
    public function setTimeout(int $timeout) : ApiRequest {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * Set the connection timeout.
     *
     * @param int $connectionTimeout
     * @return ApiRequest
     */
    public function setConnectTimeout(int $connectTimeout) : ApiRequest {
        $this->connectTimeout = $connectTimeout;
        return $this;
    }

    /**
     * Send the request.
     *
     * @return ApiResponse
     */
    public function send() : ApiResponse {
        $curl = curl_init();
        $url = $this->url;

        switch ($this->method) {
            case self::METHOD_POST:
                $this->addHeaders([ self::CONTENT_TYPE_JSON ]);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->data));
                break;
            case self::METHOD_POST_FORM:
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $this->data);
                break;
            case self::METHOD_POST_FORM_URLENCODED:
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->data));
                break;
            case self::METHOD_PUT:
                $this->addHeaders([ self::CONTENT_TYPE_JSON ]);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->data));
                break;
            case self::METHOD_PATCH:
                $this->addHeaders([ self::CONTENT_TYPE_JSON ]);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->data));
                break;
            case self::METHOD_DELETE:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                $url = sprintf("%s?%s", $this->url, http_build_query($this->data));
                break;
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);

        // Headers
        $headers = $this->_prepareHeaders($this->headers);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);

        // Send request
        $result = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
        // Save error info
        $errorCode = curl_errno($curl);
        $errorMessage = curl_error($curl);

        // Close connection
        curl_close($curl);

        // Return response
        if($result === false) {
            return new ApiResponse($errorCode, $errorMessage);
        } else {
            return new ApiResponse($status, $result);
        }
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize() {
        return get_object_vars($this);
    }

    /**
     * Parses an array into an associative array.
     * Associative arrays will be returned without
     * being modified. String arrays will be split
     * and converted into an associative array.
     *
     * @param array $raw
     * @return array
     */
    private function _parseHeaders(array $raw) : array {
        $parsed = [];

        // Return if associative
        if(ArrayHelper::isAssociative($raw)) {
            return $raw;
        }

        // Parse all headers
        foreach ($raw as $row) {
            $row = $this->_parseHeader($row);

            // Ignore if failed to parse
            if($row === null) {
                continue;
            }

            $parsed[$row[0]] = $row[1];
        }

        return $parsed;
    }

    /**
     * Parses a single header entry.
     * Array format: 0 = key, 1 = value
     *
     * @param mixed $row
     * @return array|null
     */
    private function _parseHeader($row) {
        if(is_string($row)) {
            $parts = StringHelper::split($row, ':');

            if(count($parts) == 2) {
                $key = StringHelper::trim($parts[0]);
                $value = StringHelper::trim($parts[1]);

                return [ $key, $value ];
            }
        }

        return null;
    }

    /**
     * Prepare headers for curl.
     * Converts an associative array
     * into a string array.
     *
     * @param array
     * @return array
     */
    private function _prepareHeaders(array $input) : array {
        $output = [];

        foreach ($input as $key => $value) {
            $output[] = "$key: $value";
        }

        return $output;
    }

}