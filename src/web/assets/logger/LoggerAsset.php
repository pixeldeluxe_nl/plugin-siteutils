<?php
namespace pixeldeluxe\siteutils\web\assets\logger;

use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;
use craft\web\View;

class LoggerAsset extends AssetBundle {

    /**
     * @inheritdoc
     */
    public function init() {
        $this->sourcePath = '@pixeldeluxe/siteutils/web/assets/logger/dist';
        $this->depends = [CpAsset::class];

        $this->css = [
            'styles.css',
        ];

        $this->js = [
            'scripts.js',
        ];

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function registerAssetFiles($view) {
        parent::registerAssetFiles($view);

        if ($view instanceof View) {
            $view->registerTranslations('siteutils', [
                'lang_util_logger_no_results',
                'lang_util_logger_title',
                'lang_util_logger_type',
                'lang_util_logger_date',
                'lang_util_logger_description',
                'lang_util_logger_extra'
            ]);
        }
    }

}
