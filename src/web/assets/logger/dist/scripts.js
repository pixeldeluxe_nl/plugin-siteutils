(function($) {

	Craft.Logger = Garnish.Base.extend({
		limit: 50,

		$wrapper: null,
		$loader: null,

		$container: null,

		$showArchivedContainer: null,
		$showArchivedBtn: null,
		$showArchivedCheckbox: null,

		$typeButton: null,

		$searchInput: null,
		$searchTimeout: null,

		$pagination: null,
		$paginationPrevious: null,
		$paginationNext: null,
		$paginationInfo: null,

		$archiveForm: null,
		$archiveFormBtn: null,

		$deleteForm: null,
		$deleteFormBtn: null,

		page: 1,
		type: null,
		search: null,
		archived: false,

		_loading: false,

		init: function() {
			this.$wrapper = $('#logger-list');
			this.$loader = this.$wrapper.find('.spinner');

			this.$container = this.$wrapper.find('.logger-content');

			// Select all checkbox
            this.$showArchivedContainer = this.$wrapper.find('.showarchivedcontainer');
            this.$showArchivedBtn = this.$showArchivedContainer.find('.btn');
            this.$showArchivedCheckbox = this.$showArchivedBtn.find('.checkbox');

            // Type selector
			this.$typeButton = this.$wrapper.find('#logger-filter');
			this.type = this.$typeButton.val();

            // Search input
			this.$searchInput = this.$wrapper.find('input.search');

			// Pagination tools
			this.$pagination = this.$wrapper.find('.pagination');
			this.$paginationPrevious = this.$pagination.find('.prev');
			this.$paginationNext = this.$pagination.find('.next');
			this.$paginationInfo = this.$pagination.find('.pagination-count');

			// Archive button
			this.$archiveForm = $('#logger-archive-form');
			this.$archiveFormBtn = this.$archiveForm.find('.btn');

			// Delete button
			this.$deleteForm = $('#logger-delete-form');
			this.$deleteFormBtn = this.$deleteForm.find('.btn');

			// Load container data
			this.loadData();

			// Register listeners
			this.addListener(this.$searchInput, 'keyup paste', $.proxy(function() {
				var self = this;

            	// Clear timeout on change
				if(self.$searchTimeout) {
					clearTimeout(self.$searchTimeout);
				}

				// Start timeout
				self.$searchTimeout = setTimeout(function() {
					self.search = self.$searchInput.val();
					self.loadData();
				}, 500);
			}));

            this.addListener(this.$showArchivedBtn, 'click', function() {
                this.archived = !this.archived;
                this.$showArchivedCheckbox.toggleClass('checked');
                this.loadData();
            });

			this.addListener(this.$paginationPrevious, 'click', $.proxy(function() {
				this.previousPage();
			}));

			this.addListener(this.$paginationNext, 'click', $.proxy(function() {
				this.nextPage();
			}));

			this.addListener(this.$typeButton, 'change', $.proxy(function() {
				var value = this.$typeButton.val();

				// Set type and reset page index
				this.type = value;
				this.page = 1;

				// Reload logs
				this.loadData();
			}));

            this.addListener(this.$archiveForm, 'submit', function(e) {
            	e.preventDefault();
            	this.archiveLogs(this.getCriteria());
            });

            this.addListener(this.$deleteForm, 'submit', function(e) {
            	e.preventDefault();
            	this.deleteLogs(this.getCriteria());
            });
		},

		setLoading: function(loading) {
			if(loading && !this._loading) {
				this._loading = true;
				this.$wrapper.find('.logger-control').attr('disabled', 'disabled');
				this.$wrapper.find('.logger-control').addClass('disabled');
				this.$loader.css('visibility', 'visible');
			} else if(this._loading) {
				this._loading = false;
				this.$wrapper.find('.logger-control').removeAttr('disabled');
				this.$wrapper.find('.logger-control').removeClass('disabled');
				this.$loader.css('visibility', 'hidden');
			}
		},

		isLoading: function() {
			return this._loading;
		},

		loadData: function() {
			var self = this;

			this.setLoading(true);

			$.ajax({
				url: "/actions/siteutils/logger/get-logs",
				headers: {          
					Accept: "application/json; charset=utf-8"   
				},   
				data: {
					limit: self.limit,
					offset: self.getOffset(),
					criteria: self.getCriteria()
				},
				success: function(result, status, xhr) {
					var rows = result['data'];

					// Build and set container html
					// ========================================================
					if(result.total > 0) {
						self.$container.html('');

						$.each(rows, function(i, row) {
							var $logElement = $('<div class="log-item pane"></div>').appendTo(self.$container);

							$logElement.append('<strong>' + Craft.t('siteutils', 'lang_util_logger_title') + ':</strong> ' + row.title + '<br>');
							$logElement.append('<strong>' + Craft.t('siteutils', 'lang_util_logger_type') + ':</strong> <span class="log-type" style="background: ' + row.type.color + ';">' + row.type.label + '</span><span class="type hidden">' + row.type.handle + '</span><br>');
							$logElement.append('<strong>' + Craft.t('siteutils', 'lang_util_logger_date') + ':</strong> ' + row.dateCreated + '<span class="date hidden">' + row.dateCreated + '</span><br>');

							if(row.description != null && row.description.length > 0) {
						    	$logElement.append('<strong>' + Craft.t('siteutils', 'lang_util_logger_description') + ':</strong><br> ' + row.description.replaceAll('\\n', '<br>') + '<br>');
						    }

						    if(row.extra != null && row.extra.length > 0) {
						    	$logElement.append('<strong>' + Craft.t('siteutils', 'lang_util_logger_extra') + ':</strong><br> <code>' + row.extra.replaceAll('\\n', '<br>') + '</code><br>');
						    }

						    // Add controls
						    var $logControls = $('<div class="log-controls"></div>').appendTo($logElement);
						    var $logArchive = $('<span class="icon icon-mask"><span data-icon="categories"></span></span>').appendTo($logControls);
						    var $logDelete = $('<span class="icon icon-mask"><span data-icon="trash"></span></span>').appendTo($logControls);

				            self.addListener($logArchive, 'click', function(e) {
				            	self.archiveLogs({ id: row.id });
				            });

				            self.addListener($logDelete, 'click', function(e) {
				            	self.deleteLogs({ id: row.id });
				            });
						});
					} else {
						self.$container.html('<p>Geen resultaten</p>');
					}

					// Update pagination
					// ========================================================
					var pageLimit = self.getOffset() + rows.length;
					var range = (self.getOffset() + 1) + '-' + pageLimit;

					// Update pagination info
					self.$paginationInfo.text(range + ' van de ' + result.total);

					// Disable previous button if we're viewing page one.
					if(self.page > 1) {
						self.$paginationPrevious.removeAttr('disabled');
						self.$paginationPrevious.removeClass('disabled');
					} else {
						self.$paginationPrevious.attr('disabled', 'disabled');
						self.$paginationPrevious.addClass('disabled');
					}

					// Disable next button if we're out of elements
					if(result.total > self.limit && self.getOffset(self.page) < result.total) {
						self.$paginationNext.removeAttr('disabled');
						self.$paginationNext.removeClass('disabled');
					} else {
						self.$paginationNext.attr('disabled', 'disabled');
						self.$paginationNext.addClass('disabled');
					}

					// Hide navigation if we've got no results
					self.$pagination.css('visibility', result.total <= 0 ? 'hidden' : 'visible');
				},
				error: function (xhr, status, error) {
					Craft.cp.displayError('Log index error: ' + error);
				},
				complete: function(xhr, status) {
					self.setLoading(false);
				}
			});
		},

		getOffset: function(page) {
			if(page === undefined) {
				page = (this.page - 1);
			}

			return page * this.limit;
		},

		getCriteria: function() {
			var criteria = {};

			if(this.type != null && this.type.length > 0) {
				criteria['type'] = this.type;
			}

			if(!this.archived) {
				criteria['archived'] = false;
			}

			if(this.search != null && this.search.length > 0) {
				criteria['search'] = this.search;
			}

			return criteria;
		},

		previousPage: function() {
			this.page -= 1;
			this.loadData();
		},

		nextPage: function() {
			this.page += 1;
			this.loadData();
		},

		archiveLogs: function(criteria) {
			var self = this;

			this.setLoading(true);

            Craft.postActionRequest('siteutils/logger/archive-logs', { criteria: criteria }, $.proxy(function(response, textStatus) {
                if (textStatus !== 'success') {
                    this.setLoading(false);
                    return;
                }

                self.loadData();
            }));
		},

		deleteLogs: function(criteria) {
			var self = this;

			this.setLoading(true);

            Craft.postActionRequest('siteutils/logger/delete-logs', { criteria: criteria }, $.proxy(function(response, textStatus) {
                if (textStatus !== 'success') {
                    this.setLoading(false);
                    return;
                }

                self.loadData();
            }));
		}
	});

})(jQuery);
