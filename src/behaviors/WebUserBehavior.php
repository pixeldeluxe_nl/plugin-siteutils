<?php
namespace pixeldeluxe\siteutils\behaviors;

use yii\base\Behavior;
use Craft;
use craft\elements\User;
use pixeldeluxe\siteutils\helpers\NumberHelper;

class WebUserBehavior extends Behavior {
	
	private $_impersonator;

	/**
	 * Returns whether this user is an impersonation.
	 * Meaning someone logged in as this user.
	 *
	 * @return bool
	 */
	public function getIsImpersonation() : bool {
		return $this->getImpersonator() !== null;
	}

	/**
	 * Returns the user who is impersonating another user.
	 * Null will be returned when the web user isn't impersonating
	 * or if the the user can not be found.
	 *
	 * @return User|null
	 */
	public function getImpersonator() {
		if($this->_impersonator !== null) {
			return $this->_impersonator === false ? null : $this->_impersonator;
		}

		// Return null if not an impersonation
		if(!Craft::$app->session->has(User::IMPERSONATE_KEY)) {
			$this->_impersonator = false;
			return null;
		}

		// Parse session value and find user
		$userId = Craft::$app->session->get(User::IMPERSONATE_KEY);
		$user = NumberHelper::isInt($userId) ? Craft::$app->users->getUserById(intval($userId)) : null;

		// Cache and return
		$this->_impersonator = $user === null ? false : $user;
		return $user;
	}

}