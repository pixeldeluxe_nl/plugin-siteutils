<?php
namespace pixeldeluxe\siteutils\behaviors;

use yii\base\Behavior;
use Solspace\Freeform\Freeform;

class LinkBehavior extends Behavior {
	
    /**
     * Returns whether this link is a form.
     *
     * @return bool
     */
	public function isForm() : bool {
		return substr($this->owner->getUrl(), 0, 6) === 'modal:';
	}

    /**
     * Returns the selected form or null.
     *
     * @return FormModel|null
     */
	public function getForm() {
		if(!$this->isForm()) {
			return null;
		}

		// Get the form id
		$data = explode(':', $this->owner->getUrl());
		$id = intval($data[1]);

		// Return form by id
		return Freeform::getInstance()->forms->getFormById($id);
	}

}