<?php
namespace pixeldeluxe\siteutils\helpers;

use Throwable;
use TypeError;

class TaskHelper {

    /**
     * Executes a callable inside of a try catch.
     * onError will be called if any throwables are, well, thrown, duh.
     * 
     * @param callable $task
     * @param callable $onError
     */
    public static function executeSafe(callable $task, $onError = null) {
        try {
            $task();
        } catch(Throwable $e) {
            if($onError !== null) {
                if(!is_callable($onError)) {
                    throw new TypeError("onError must be callable");
                }

                $onError($e);
            }
        }
    }

}