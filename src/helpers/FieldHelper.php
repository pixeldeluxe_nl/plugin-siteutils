<?php
namespace pixeldeluxe\siteutils\helpers;

use craft\fields\data\SingleOptionFieldData;
use craft\fields\data\MultiOptionsFieldData;

class FieldHelper {

    /**
     * Returns the value of a single option field.
     *
     * @param SingleOptionFieldData $data
     * @return string
     */
    public static function getSingleOptionValue(SingleOptionFieldData $data, bool $useLabel = false, $default = null) {
        $property = $useLabel ? 'label' : 'value';
        return $data->$property ?? $default;
    }

    /**
     * Returns the value of a multi option field.
     *
     * @param MultiOptionsFieldData $data
     * @return array|string|null
     */
    public static function getMultiOptionValue(MultiOptionsFieldData $data, bool $useLabel = false, $default = null) {
        $property = $useLabel ? 'label' : 'value';
        $selected = [];

        foreach ($data->getOptions() as $option) {
            if(!$option->selected) {
                continue;
            }
            
            $selected[] = $option->$property;
        }

        // Return all options if more than one
        if(count($selected) > 1) {
            return $selected;
        }

        return reset($selected) ?? $default;
    }

}