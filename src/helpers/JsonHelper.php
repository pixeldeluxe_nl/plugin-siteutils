<?php
namespace pixeldeluxe\siteutils\helpers;

use stdClass;
use yii\helpers\Json;
use yii\base\InvalidArgumentException;

class JsonHelper {
	
    /**
     * Returns an object with the json properties.
     *
     * @param mixed $json
     * @return mixed|null
     */
    public static function decode($json) {
        // Return null if null
        if($json === null) {
            return $json;
        }

    	// Return if its already an object
    	if(is_object($json)) {
    		return $json;
    	}

    	// Create object with properties if its an array
    	if(is_array($json)) {
    		$object = new stdClass();

    		// Set properties
    		foreach ($json as $key => $value) {
    			$object->$key = $value;
    		}

    		return $object;
    	}

    	// Make sure its a string before passing it
    	if(!is_string($json)) {
    		throw new InvalidArgumentException("The json parameter must be an object, array or string");
    	}

    	// Create object from json string
        try {
            return Json::decode($json, false);
        } catch (InvalidArgumentException $e) {
            return null;
        }
	}

    /**
     * Returns whether the specified json is valid.
     *
     * @param mixed $json
     * @return bool
     */
	public static function isValid($json) : bool {
		return self::decode($json) !== null;
	}

    /**
     * Returns a property from the specified json.
     *
     * @param mixed $json
     * @param mixed $key
     * @param mixed $default
     * @return mixed
     */
	public static function getProperty($json, $key, $default = null) {
		$json = self::decode($json);

		if($json === null) {
			throw new InvalidArgumentException("Invalid json");
		}

		return isset($json->$key) ? $json->$key : $default;
	}

}