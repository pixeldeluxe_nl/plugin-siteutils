<?php
namespace pixeldeluxe\siteutils\helpers;

class ArrayHelper extends \craft\helpers\ArrayHelper {

    public static function filterObject(array $array, $filter) {
        foreach($array as $item) {
            if($filter($item)) {
                return $item;
            }
        }

        return null;
    }

    public static function findObject(array $array, string $property, $value, bool $strict = true) {
        return static::filterObject($array, function($item) use ($property, $value, $strict) {
            $itemValue = $item->$property;

            if(($strict && $itemValue === $value) || (!$strict && $itemValue == $value)) {
                return $item;
            }
        });
    }

}