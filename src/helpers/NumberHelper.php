<?php
namespace pixeldeluxe\siteutils\helpers;

class NumberHelper {
	
    /**
     * Returns whether the specified value is
     * a valid integer.
     *
     * @param mixed $input
     * @return bool
     */
	public static function isInt($input) : bool {
		return intval($input) != 0 || $input == 0;
	}

}