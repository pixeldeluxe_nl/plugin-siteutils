<?php
namespace pixeldeluxe\siteutils\helpers;

use Exception;
use Craft;
use craft\elements\Asset;
use craft\helpers\Template as TemplateHelper;
use craft\mail\Message;
use craft\volumes\Local;

class MailHelper {

    /**
     * Sends a plain text email using the default template.
     *
     * @param string $mail
     * @param string $subject
     * @param string $body
     * @param array $attachments
     * @return bool
     */
    public static function sendTextMail(string $mail, string $subject, string $body, $attachments = null): bool {
        $settings = Craft::$app->systemSettings->getSettings('email');
        $message = new Message();

        // Build message
        $message->setFrom([Craft::parseEnv($settings['fromEmail']) => Craft::parseEnv($settings['fromName'])]);
        $message->setTo($mail);
        $message->setSubject($subject);

        // Get system template
        $template = Craft::parseEnv($settings['template']);

        // Use system email template if it exists
        if(!empty($template)) {
            $view = Craft::$app->getView();
            $oldTemplateMode = $view->getTemplateMode();

            // Set template mode
            $view->setTemplateMode($view::TEMPLATE_MODE_SITE);

            // Check if the system template exists 
            if (!$view->doesTemplateExist($template)) {
                $view->setTemplateMode($oldTemplateMode);
                throw new Exception("System email template doesn't exist");
            }

            // Render system template with body
            $body = Craft::$app->getView()->renderTemplate($template, [
                'body' => TemplateHelper::raw($body)
            ]);

            // Restore template mode
            $view->setTemplateMode($oldTemplateMode);
        }

        // Set body
        $message->setHtmlBody($body);

        // Attach files
        if($attachments !== null) {
            // Make sure its an array
            if(!is_array($attachments)) {
                $attachments = [$attachments];
            }

            // Loop through attachments
            foreach ($attachments as $attachment) {
                $message->attach(static::_parseAttachment($attachment));
            }
        }

        // Send it
        return Craft::$app->mailer->send($message);
    }

    /**
     * Sends an email using the specified template.
     *
     * @param string $mail
     * @param string $subject
     * @param string $template
     * @param array $variables
     * @param mixed $attachments
     * @return bool
     */
    public static function sendTemplateMail(string $mail, string $subject, string $template, array $variables, $attachments = null): bool {
        $view = Craft::$app->getView();
        $oldTemplateMode = $view->getTemplateMode();

        // Set template mode
        $view->setTemplateMode($view::TEMPLATE_MODE_CP);

        // Check if the template exists
        if (!$view->doesTemplateExist($template)) {
            $view->setTemplateMode($oldTemplateMode);
            throw new Exception("Template doesn't exist");
        }

        // Try to render it
        try {
            $body = $view->renderTemplate($template, $variables);
        } catch (Exception $e) {
            $view->setTemplateMode($oldTemplateMode);
            throw new Exception("Unable to render template " . $e->getMessage());
        }

        // Reset template mode
        $view->setTemplateMode($oldTemplateMode);

        // Send it
        return static::sendTextMail($mail, $subject, $body, $attachments);
    }

    /**
     * Creates a list of file paths and options
     *
     * @param mixed $attachments
     */
    private static function _parseAttachment($attachment) {
        if($attachment instanceof Asset) {
            if($attachment->getVolume() instanceof Local) {
                return $attachment->getVolume()->getRootPath() . '/' . $attachment->getPath();                
            }

            throw new Exception('Asset must be on local volume: ' . $attachment->title);
        } elseif(is_string($attachment) && file_exists($attachment)) {
            return $attachment;
        } else {
            throw new Exception("Invalid attachment: $attachment");
        }
    }

}