<?php
namespace pixeldeluxe\siteutils\helpers;

use ReflectionClass;

class ClassHelper {

    public static function getClassName(object $object) {
        $class = new ReflectionClass($object);
        return $class->getShortName();
    }

}