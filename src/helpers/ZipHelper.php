<?php
namespace pixeldeluxe\siteutils\helpers;

class ZipHelper {

    /**
     * Creates a zip with the source files at the specified destination.
     *
     * @param string $source
     * @param string $destination
     * @param bool $includeDir
     * @param array $ignored
     * @return mixed|null
     */
	public static function zip(string $source, string $destination, bool $includeDir = false, array $ignored = []) : bool {
	    if (!extension_loaded('zip') || !file_exists($source)) {
	        return false;
	    }

	    if (file_exists($destination)) {
	        unlink ($destination);
	    }

	    $zip = new \ZipArchive();
	    if (!$zip->open($destination, \ZIPARCHIVE::CREATE)) {
	        return false;
	    }
	    $source = str_replace('\\', '/', realpath($source));

	    if (is_dir($source) === true) {
			$filter = function ($file, $key, $iterator) use ($ignored) {
			    if ($iterator->hasChildren() && !in_array($file->getFilename(), $ignored)) {
			        return true;
			    }
			    return $file->isFile();
			};

			$innerIterator = new \RecursiveDirectoryIterator($source);
			$files = new \RecursiveIteratorIterator(
			    new \RecursiveCallbackFilterIterator($innerIterator, $filter), 
			    \RecursiveIteratorIterator::SELF_FIRST
			);

	        if ($includeDir) {
	            $arr = explode("/",$source);
	            $maindir = $arr[count($arr)- 1];

	            $source = "";
	            for ($i=0; $i < count($arr) - 1; $i++) { 
	                $source .= '/' . $arr[$i];
	            }

	            $source = substr($source, 1);
	            $zip->addEmptyDir($maindir);
	        }

	        foreach ($files as $file) {
	            $file = str_replace('\\', '/', $file);

	            // Ignore "." and ".." folders
	            if(in_array(substr($file, strrpos($file, '/')+1), array('.', '..'))) continue;

	            $file = realpath($file);

	            if (is_dir($file) === true && !in_array(basename($file), $ignored)) {
	                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
	            } else if (is_file($file) === true && !in_array(basename($file), $ignored)) {
	                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
	            }
	        }
	    } else if (is_file($source) === true) {
	        $zip->addFromString(basename($source), file_get_contents($source));
	    }

	    return $zip->close();
	}

}