<?php
namespace pixeldeluxe\siteutils\helpers;

use pixeldeluxe\siteutils\base\HashMap;

class MapHelper {
	
    /**
     * Create a map from an array.
     *
     * @param array $array
     * @param string $keyHandle
     * @return HashMap
     */
	public static function createFromArray(array $array, string $keyHandle = 'id') : HashMap {
		$map = new HashMap();

		foreach ($array as $value) {
			$map->set($value->$keyHandle, $value);
		}

		return $map;
	}

}