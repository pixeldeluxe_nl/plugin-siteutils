<?php
namespace pixeldeluxe\siteutils\services;

use yii\base\Component;
use pixeldeluxe\siteutils\base\HashMapTrait;

class ContextStorage extends Component {

	private $map = [];

	use HashMapTrait;

}