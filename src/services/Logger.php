<?php
namespace pixeldeluxe\siteutils\services;

use InvalidArgumentException;
use yii\base\Component;
use Craft;
use craft\helpers\ArrayHelper;
use craft\helpers\DateTimeHelper;
use pixeldeluxe\siteutils\base\HashMap;
use pixeldeluxe\siteutils\records\LogRecord;
use pixeldeluxe\siteutils\enums\LogType;
use pixeldeluxe\siteutils\models\LogType as LogTypeModel;
use pixeldeluxe\siteutils\models\Log as LogModel;
use pixeldeluxe\siteutils\events\collection\RegisterLogTypesEvent;

class Logger extends Component {

    const EVENT_REGISTER_LOG_TYPES = 'registerLogTypes';

    private $logTypes;

    /**
     * @inheritdoc
     */
    public function init() {
        $logTypes = [];

        parent::init();

        // Default log types
        $logTypes[] = new LogTypeModel(['handle' => LogType::INFO, 'label' => 'Info', 'color' => '#60A4C3']);
        $logTypes[] = new LogTypeModel(['handle' => LogType::ERROR, 'label' => 'Error', 'color' => '#DC4E4D']);
        $logTypes[] = new LogTypeModel(['handle' => LogType::EXCEPTION, 'label' => 'Exception', 'color' => '#394AC6']); 
        $logTypes[] = new LogTypeModel(['handle' => LogType::WARNING, 'label' => 'Warning', 'color' => '#DF931A']);

        // Register debug when its enabled
        if(Craft::$app->config->general->devMode) {
            $logTypes[] = new LogTypeModel(['handle' => LogType::DEBUG, 'label' => 'Debug', 'color' => '#60A4C3']);
        }

        // Fire a 'registerLogTypes' event
        $event = new RegisterLogTypesEvent([ 'logTypes' => $logTypes ]);

        if ($this->hasEventHandlers(self::EVENT_REGISTER_LOG_TYPES)) {
            $this->trigger(self::EVENT_REGISTER_LOG_TYPES, $event);
        }

        // Map logtypes
        $this->logTypes = new HashMap();

        foreach ($event->logTypes as $logType) {
            $this->logTypes->set($logType->handle, $logType);
        }
    }

    /**
     * Returns all log types.
     *
     * @return LogTypeModel[]
     */
    public function getLogTypes() : array {
        return $this->logTypes->getValues();
    }

    /**
     * Create a new log record.
     *
     * @param string $type
     * @param string $title
     * @param string|null $description
     * @param mixed|null $extra
     * @return bool
     */
    public function log(string $type, string $title, string $description = null, $extra = null) {
        if(!in_array($type, $this->logTypes->getKeys())) {
            return;
        }

        // Create and save record
        $record = new LogRecord();
        $record->type = $type;
        $record->title = $title;
        $record->description = $description;
        $record->extra = $extra !== null ? json_encode($extra, JSON_UNESCAPED_SLASHES) : null;
        $record->save();
    }

    /**
     * Get all logs from a specific type.
     *
     * @param int $limit
     * @param int $offset
     * @param array criteria
     * @return LogRecord[]
     */
    public function getLogs(int $limit = null, int $offset = null, array $criteria) {
        $records = $this->_getListQuery($limit, $offset, $criteria)->all();
        return $this->_createLogModels($records);
    }

    /**
     * Returns the total amount of logs for the specified criteria.
     *
     * @param array $criteria
     * @return int
     */
    public function getTotal(array $criteria) : int {
        return $this->_getBaseQuery($criteria)->count();
    }

    /**
     * Archive all logs matching the specified criteria.
     *
     * @param array $criteria
     * @return int The numer of affected rows
     */
    public function archiveLogs(array $criteria) {
        $logIds = $this->_getBaseQuery($criteria)->select('id')->column();
        return LogRecord::updateAll(['archived' => true], [ 'in', 'id', $logIds ]);
    }

    /**
     * Archive all logs matching the specified criteria.
     *
     * @param array $criteria
     * @return int The numer of affected rows
     */
    public function deleteLogs(array $criteria) {
        $logIds = $this->_getBaseQuery($criteria)->select('id')->column();
        return LogRecord::deleteAll([ 'in', 'id', $logIds ]);
    }

    /**
     * Returns the base query used to query logs.
     *
     * @param array criteria
     * @return Query
     */
    private function _getBaseQuery(array $criteria) {
        $search = ArrayHelper::remove($criteria, 'search');

        // Create query
        $query = LogRecord::find();

        // Apply search condition
        if(!empty($search)) {
            $query->andWhere(['or', ['LIKE', 'title', $search], ['LIKE', 'description', $search], ['LIKE', 'extra', $search]]);
        }

        // Apply remaning criteria
        $query->andWhere($criteria);

        // Only fetch registered log types
        $query->andWhere([ 'in', 'type', $this->logTypes->getKeys() ]);

        // Sort order
        $query->orderBy([ 'id' => SORT_DESC ]);

        return $query;
    }

    /**
     * Returns the list query used to query logs.
     *
     * @param int|null limit
     * @param int|null offset
     * @param array criteria
     * @return Query
     */
    private function _getListQuery(int $limit = null, int $offset = null, array $criteria) {
        $query = $this->_getBaseQuery($criteria);

        // Limit
        if($limit != null) {
            $query->limit($limit);
        }

        // Offset
        if($offset != null) {
            $query->offset($offset);
        }

        return $query;
    }

    /**
     * Turns an LogRecord array into a LogModel array.
     *
     * @param array $records
     * @return LogModel[]
     */
    private function _createLogModels(array $records) : array {
        $models = [];

        foreach ($records as $record) {
            $log = new LogModel();

            $log->id = $record->id;
            $log->type = $this->logTypes->get($record->type);
            $log->title = $record->title;
            $log->description = $record->description;
            $log->extra = $record->extra;
            $log->dateCreated = DateTimeHelper::toDateTime($record->dateCreated);
            $log->archived = $record->archived;

            $models[] = $log;
        }

        return $models;
    }

}