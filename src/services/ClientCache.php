<?php
namespace pixeldeluxe\siteutils\services;

use yii\base\Component;
use Craft;
use pixeldeluxe\siteutils\records\CacheTokenRecord;

class ClientCache extends Component {

    /**
     * Returns the current token or a random token
     * when caching is disabled.
     *
     * @return string
     */
    public function getToken() : string {
        // Return a random string if caching is disabled
        if(!Craft::$app->config->general->cache) {
            return $this->_generateToken();
        }

        // Fetch existing record
        $record = CacheTokenRecord::find()->one();

    	// Generate new token if record doesn't exist
    	if($record === null) {
    		return $this->regenerateToken();
    	}

    	return $record->token;
    }

    /**
     * Generates, saves and returns a new token.
     *
     * @return string
     */
    public function regenerateToken() : string {
		$token = $this->_generateToken();
	
        // Fetch the existing record or create one
        $record = CacheTokenRecord::find()->one();

        if($record === null) {
            $record = new CacheTokenRecord();
        }

        // Update and save it
        $record->token = $token;
        $record->save();

        // Return new token
		return $token;
    }

    /**
     * Returns a random token.
     *
     * @return string
     */
    private function _generateToken() : string {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

}