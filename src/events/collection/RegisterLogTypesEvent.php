<?php
namespace pixeldeluxe\siteutils\events\collection;

use yii\base\Event;

class RegisterLogTypesEvent extends Event {

	public $logTypes;

}