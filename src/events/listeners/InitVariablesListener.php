<?php
namespace pixeldeluxe\siteutils\events\listeners;

use yii\base\Event;
use craft\web\twig\variables\CraftVariable;
use pixeldeluxe\siteutils\events\EventListener;
use pixeldeluxe\siteutils\web\twig\CraftVariableBehavior as SiteUtilsVariable;

class InitVariablesListener extends EventListener {

    /**
     * @inheritdoc
     */
    public function getEventClass() {
        return CraftVariable::class;
    }

    /**
     * @inheritdoc
     */
    public function getEventName() {
        return CraftVariable::EVENT_INIT;
    }

    /**
     * @inheritdoc
     */
	public function onEvent(Event $e) {
        $variable = $e->sender;
        $variable->attachBehavior('siteutils', SiteUtilsVariable::class);      
	}

}