<?php
namespace pixeldeluxe\siteutils\events\listeners;

use Throwable;
use yii\base\Event;
use yii\web\HttpException;
use craft\web\ErrorHandler;
use pixeldeluxe\siteutils\SiteUtils;
use pixeldeluxe\siteutils\enums\LogType;
use pixeldeluxe\siteutils\events\EventListener;

class BeforeHandleExceptionListener extends EventListener {

    /**
     * @inheritdoc
     */
    public function getEventClass() {
        return ErrorHandler::class;
    }

    /**
     * @inheritdoc
     */
    public function getEventName() {
        return ErrorHandler::EVENT_BEFORE_HANDLE_EXCEPTION;
    }

    /**
     * @inheritdoc
     */
	public function onEvent(Event $event) {
        $e = $event->exception;

        // Log non-ignored exceptions
        if (!$this->_shouldIgnoreException($e)) {
            SiteUtils::getInstance()->logger->log(LogType::EXCEPTION, "Exception " . get_class($e) . " thrown", $e->getMessage(), [
                'exception' => $e,
                'stacktrace' => $e->getTraceAsString()
            ]);
        }
	}

    private function _shouldIgnoreException(Throwable $exception) {
        if ($exception instanceof HttpException) {
            $statusCode = $exception->statusCode;

            // Ignore client errors
            if(($statusCode >= 400 && $statusCode < 500) || $statusCode == 200) {
                return true;
            }
        }

        // Check recursively
        $previous = $exception->getPrevious();

        if($previous !== null) {
            return $this->_shouldIgnoreException($previous);
        }

        return false;
    }

}