<?php
namespace pixeldeluxe\siteutils\events\listeners;

use yii\base\Event;
use pixeldeluxe\siteutils\events\EventListener;
use pixeldeluxe\siteutils\fields\FormLinkType;
use typedlinkfield\Plugin as LinkPlugin;

class RegisterLinkTypeListener extends EventListener {

    /**
     * Returns the class the event is triggered from.
     *
     * @return string
     */
    public function getEventClass() {
        return LinkPlugin::class;
    }

    /**
     * Returns the event name.
     *
     * @return string
     */
    public function getEventName() {
        return LinkPlugin::EVENT_REGISTER_LINK_TYPES;
    }

    /**
     * Register the link type fields.
     *
     * @param Event $event
     */
	public function onEvent(Event $event) {
        $event->linkTypes['freeForm'] = new FormLinkType();
	}

}