<?php
namespace pixeldeluxe\siteutils\events\listeners;

use yii\base\Event;
use craft\services\Utilities;
use pixeldeluxe\siteutils\events\EventListener;
use pixeldeluxe\siteutils\utilities\Logger;

class RegisterUtilitiesListener extends EventListener {

    /**
     * @inheritdoc
     */
    public function getEventClass() {
        return Utilities::class;
    }

    /**
     * @inheritdoc
     */
    public function getEventName() {
        return Utilities::EVENT_REGISTER_UTILITY_TYPES;
    }

    /**
     * @inheritdoc
     */
	public function onEvent(Event $e) {
        $e->types[] = Logger::class;
	}

}