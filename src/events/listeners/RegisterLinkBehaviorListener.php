<?php
namespace pixeldeluxe\siteutils\events\listeners;

use yii\base\Event;
use pixeldeluxe\siteutils\events\EventListener;
use pixeldeluxe\siteutils\behaviors\LinkBehavior;
use typedlinkfield\models\Link;

class RegisterLinkBehaviorListener extends EventListener {

    /**
     * Returns the class the event is triggered from.
     *
     * @return string
     */
    public function getEventClass() {
        return Link::class;
    }

    /**
     * Returns the event name.
     *
     * @return string
     */
    public function getEventName() {
        return Link::EVENT_DEFINE_BEHAVIORS;
    }

    /**
     * Register the Link behavior.
     *
     * @param Event $event
     */
	public function onEvent(Event $event) {
        $event->behaviors[] = LinkBehavior::class;
	}

}