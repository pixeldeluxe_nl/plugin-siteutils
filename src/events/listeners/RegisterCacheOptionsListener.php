<?php
namespace pixeldeluxe\siteutils\events\listeners;

use yii\base\Event;
use Craft;
use craft\utilities\ClearCaches;
use pixeldeluxe\siteutils\SiteUtils;
use pixeldeluxe\siteutils\events\EventListener;

class RegisterCacheOptionsListener extends EventListener {

    /**
     * @inheritdoc
     */
    public function getEventClass() {
        return ClearCaches::class;
    }

    /**
     * @inheritdoc
     */
    public function getEventName() {
        return ClearCaches::EVENT_REGISTER_CACHE_OPTIONS;
    }

    /**
     * @inheritdoc
     */
	public function onEvent(Event $e) {
        // Register client cache option
        $e->options[] = [
            'key' => 'client-cache',
            'label' => Craft::t('siteutils', 'lang_cache_option_client_cache'),
            'action' => function() {
                SiteUtils::getInstance()->clientCache->regenerateToken();
            }
        ];        
	}

}