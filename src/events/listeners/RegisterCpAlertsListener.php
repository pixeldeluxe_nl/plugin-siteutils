<?php
namespace pixeldeluxe\siteutils\events\listeners;

use yii\base\Event;
use Craft;
use craft\helpers\Cp;
use pixeldeluxe\siteutils\events\EventListener;

class RegisterCpAlertsListener extends EventListener {

    public function getEventClass() {
        return Cp::class;
    }
    
    public function getEventName() {
        return Cp::EVENT_REGISTER_ALERTS;
    }
    
	public function onEvent(Event $event) {
        $user = Craft::$app->user->getIdentity();
        $config = Craft::$app->getConfig();
        $general = $config->getGeneral();

        // Add admin changes alert
        if($config->env != 'local' && $general->allowAdminChanges && $user !== null && $user->admin) {
            $event->alerts[] = Craft::t('siteutils', 'lang_alert_admin_changes');
        }
	}

}