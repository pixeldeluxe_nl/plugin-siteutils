<?php
namespace pixeldeluxe\siteutils\events;

use yii\base\Event;

abstract class EventListener {

	public static $instance = array();

    /**
     * Returns the class the event is triggered from.
     *
     * @return string
     */
	public abstract function getEventClass();

    /**
     * Returns the event name.
     *
     * @return string
     */
	public abstract function getEventName();

    /**
     * Call the event callback when the even is triggered.
     */
	public abstract function onEvent(Event $e);

	public static function init() {
	    $className = get_called_class();
	    EventListener::$instance[$className] = new $className();

        Event::on(EventListener::$instance[$className]->getEventClass(), EventListener::$instance[$className]->getEventName(), function(Event $e) {
        	EventListener::$instance[get_called_class()]->onEvent($e);
        });
	}

}