<?php
namespace pixeldeluxe\siteutils\records;

use craft\db\ActiveRecord;
use pixeldeluxe\siteutils\db\Table;

class CacheTokenRecord extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return Table::CACHE_TOKEN;
    }

}
