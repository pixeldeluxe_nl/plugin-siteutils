<?php
namespace pixeldeluxe\siteutils\records;

use craft\db\ActiveRecord;
use pixeldeluxe\siteutils\db\Table;

class LogRecord extends ActiveRecord implements \JsonSerializable {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return Table::LOGS;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize() {
        return get_object_vars($this);
    }

}