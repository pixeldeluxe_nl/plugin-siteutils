<?php
namespace pixeldeluxe\siteutils\utilities;

use Craft;
use craft\base\Utility;
use pixeldeluxe\siteutils\web\assets\logger\LoggerAsset;

class Logger extends Utility {

    /**
     * @inheritdoc
     */
	public static function displayName(): string {
		return Craft::t('siteutils', 'lang_util_logger_name');
	}

    /**
     * @inheritdoc
     */
	public static function id(): string {
		return 'logger';
	}

    /**
     * @inheritdoc
     */
	public static function iconPath() {
		return Craft::getAlias('@pixeldeluxe/siteutils/icons/logs.svg');
	}

    /**
     * @inheritdoc
     */
	public static function contentHtml(): string {
		return Craft::$app->getView()->renderTemplate(
			'siteutils/utilities/logger'
		);
	}

}