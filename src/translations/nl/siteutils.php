<?php

return [

	'lang_cache_option_client_cache'   => 'Client cache',

	'lang_util_logger_name'            => 'Logger',
	'lang_util_logger_archive_success' => "Alle logs zijn gearchiveerd",
	'lang_util_logger_archive_info'    => "Toon gearchiveerd",
	'lang_util_logger_delete_success'  => "Alle logs zijn verwijderd",
	'lang_util_logger_type'            => "Type",
	'lang_util_logger_date'            => "Datum",
	'lang_util_logger_title'           => "Titel",
	'lang_util_logger_description'     => "Beschrijving",
	'lang_util_logger_extra'           => "Extra data",
	'lang_util_logger_no_results'      => "Geen logs gevonden",

    'lang_alert_admin_changes'         => "Admin changes staan aan op een niet-lokale omgeving"

];
