<?php

return [

	'lang_cache_option_client_cache'   => 'Client cache',

	'lang_util_logger_name'            => 'Logger',
	'lang_util_logger_archive_success' => "All logs have been archived",
	'lang_util_logger_archive_info'    => "Show archived",
	'lang_util_logger_delete_success'  => "All logs have been deleted",
	'lang_util_logger_type'            => "Type",
	'lang_util_logger_date'            => "Date",
	'lang_util_logger_title'           => "Title",
	'lang_util_logger_description'     => "Description",
	'lang_util_logger_extra'           => "Extra data",
	'lang_util_logger_no_results'      => "No logs found",

    'lang_alert_admin_changes'         => "Admin changes are enabled on a non-local environment"

];
