<?php
namespace pixeldeluxe\siteutils\fields;

use Throwable;
use yii\base\Model;
use Craft;
use craft\base\ElementInterface;
use typedlinkfield\models\LinkTypeInterface;
use typedlinkfield\fields\LinkField;
use typedlinkfield\models\Link;
use Solspace\Freeform\Freeform;

class FormLinkType extends Model implements LinkTypeInterface {

	/**
	* @inheritdoc
	*/
	public function getDefaultSettings(): array {
		return [
			'allowAliases'      => false,
			'disableValidation' => false,
		];
	}

    /**
     * @inheritdoc
     */
	public function getDisplayName() : string {
		return 'Formulier';
	}

    /**
     * @inheritdoc
     */
	public function getDisplayGroup() : string {
		return 'FreeForm';
	}

	/**
	* Returns a form by the field value
	*
	* @param Link $link
	* @return null|FreeForm
	*/
	public function getForm(Link $link) {
		if ($this->isEmpty($link)) {
			return null;
		}

		return Freeform::getInstance()->forms->getFormById($link->value);
	}

    /**
     * @inheritdoc
     */
	public function getInputHtml(string $linkTypeName, LinkField $field, Link $value, ElementInterface $element = null): string {
		$isSelected   = $value->type === $linkTypeName;
		$selectedForm = $isSelected ? $this->getForm($value) : null;
		
		$selectFieldOptions = [
			'disabled' => $field->isStatic(),
			'id'       => $field->handle . '-' . $linkTypeName,
			'name'     => $field->handle . '[' . $linkTypeName . ']',
			'options'  => $this->getFormOptions(),
			'value'    => $selectedForm->id ?? null,
		];

		try {
			return Craft::$app->view->renderTemplate('typedlinkfield/_input-select', [
				'isSelected'         => $isSelected,
				'linkTypeName'       => $linkTypeName,
				'selectFieldOptions' => $selectFieldOptions,
			]);
		} catch (Throwable $exception) {
			return null;
		}
	}

    /**
     * @inheritdoc
     */
	public function getLinkValue($value) {
		return $value ?? null;
	}

    /**
     * @inheritdoc
     */
	public function getSettingsHtml(string $linkTypeName, LinkField $field): string {
		return "";
	}

    /**
     * Get all freeform's as option array.
     *
     * @return array
     */
	private function getFormOptions() {
		$forms = Freeform::getInstance()->forms->getAllForms();
		$options = [];

		foreach ($forms as $form) {
			array_push($options, [
                'label' => $form->name,
                'value' => $form->id,
                'default' => ''
			]);
		}

		return $options;
	}

    /**
     * @inheritdoc
     */
	public function getText(Link $link) {
		$form = $this->getForm($link);

		if (is_null($form)) {
			return null;
		}

		return $form->name;
	}

    /**
     * @inheritdoc
     */
	public function getUrl(Link $link) {
		$form = $this->getForm($link);

		if (is_null($form)) {
			return null;
		}

		return 'modal:' . $form->id;
	}

    /**
     * @inheritdoc
     */
	public function isEmpty(Link $link): bool {
		if (is_string($link->value)) {
			return trim($link->value) === '';
		}

		return true;
	}

    /**
     * @inheritdoc
     */
	public function validateSettings(array $settings): array {
		return $settings;
	}

    /**
     * @inheritdoc
     */
	public function validateValue(LinkField $field, Link $link) {
		return null;
	}

    /**
     * @inheritdoc
     */
	public function getElement(Link $link, $ignoreStatus = false) {
		return null;
	}

    /**
     * @inheritdoc
     */
	public function hasElement(Link $link, $ignoreStatus = false): bool {
		return false;
	}

  /**
   * @inheritDoc
   */
  public function readLinkValue($formData) {
    return is_string($formData) ? $formData : '';
  }

}